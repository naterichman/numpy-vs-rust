annotation_handler @ file:///tmp/.tmpq8sSbQ/annotation_handler-0.1.0-cp38-cp38-linux_x86_64.whl
imageio==2.27.0
lazy_loader==0.2
maturin==0.14.16
networkx==3.1
numpy==1.24.2
packaging==23.0
Pillow==9.5.0
py-spy==0.3.14
PyWavelets==1.4.1
scikit-image==0.20.0
scipy==1.9.1
tifffile==2023.3.21
tomli==2.0.1
