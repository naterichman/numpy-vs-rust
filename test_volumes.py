import json
from annotation_handler import process_volumes, process_layers
import time
import numpy as np
from skimage.draw import polygon as draw_polygon
from matplotlib import pyplot as plt

def python_version(filename, x, y, z):
    with open(filename, 'r') as fp:
        annotations = json.load(fp)
    volume = np.zeros((x,y,z), dtype=np.int8)
    for annotation in annotations:
        data = annotation["data"]
        z_index = data["sliceNumber"]
        points = np.array(
            [
                [np.round(p["x"]), np.round(p["y"]), z_index]
                for p in data["handles"]["points"]
            ]
        ).astype(int)
        row_coords, col_coords = draw_polygon(points[:, 0], points[:, 1], (x,y,z))
        volume[row_coords, col_coords, z_index] = 1
    return volume



if __name__ == '__main__':

    filename = 'IRF.json'
    with open(filename, 'r') as fp:
        annotations = json.load(fp)

# We are going to increase this as the code gets faster and faster.
    NUM_ITER = 100

    t0 = time.perf_counter()
    for _ in range(NUM_ITER):
        r_out = process_volumes(filename, 1024, 496, 97)
    t1 = time.perf_counter()
    #fig, ax = plt.subplots(1, 2)
    #ax[0].imshow(r_out[:,:,50])
    #ax[0].set_title('Rust')

    took = (t1 - t0) / NUM_ITER
    print(f"Rust: took and avg of {took * 1000:.2f}ms per iteration")

    t0 = time.perf_counter()
    for _ in range(NUM_ITER):
        out =python_version(filename, 1024, 496, 97)
    #ax[1].imshow(out[:,:,50])
    #ax[1].set_title('Python')
    #plt.show()
    t1 = time.perf_counter()
    took = (t1 - t0) / NUM_ITER

    print(f"Python: took and avg of {took * 1000:.2f}ms per iteration")
