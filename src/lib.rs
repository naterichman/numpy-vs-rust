use pyo3::prelude::*;
use ndarray::prelude::*;
use ndarray::Zip;
use std::{io::{BufReader, Read}, collections::HashMap, fs::{File, OpenOptions}, time::Instant};
use geo_types::{LineString, Polygon};
use geo_rasterize::BinaryBuilder;
use numpy::{IntoPyArray, PyArray3, PyArray2};
use rayon::prelude::*;

mod annotations;
mod layers;
use annotations::Annotation;
use layers::{construct_layer, construct_layers, layer_thicknesses};

/// Formats the sum of two numbers as string.
#[pyfunction]
fn sum_as_string(a: usize, b: usize) -> PyResult<String> {
    Ok((a + b).to_string())
}

#[pyfunction]
fn process_layers(
    py: Python<'_>, annotations: String, x: usize, _y: usize, z: usize
) -> PyResult<HashMap<String, &PyArray2<f64>>> {
    //let tic = Instant::now();
    let layer_order: Vec<String> = vec!["BM","IB-RPE","EZ","ELM","OPL-HFL","ILM"]
        .iter().map(|v| v.to_string()).collect();
    let mut s = String::new();
    //println!("Opening file: {:?}ms", tic.elapsed().as_millis());
    File::open(annotations).unwrap().read_to_string(&mut s).unwrap();
    let annotations: Vec<Annotation> = serde_json::from_str(&s).unwrap();
    let mut grouped_annotations = HashMap::<usize, HashMap<String, Annotation>>::new();
    annotations.into_iter().for_each(|a| {
        grouped_annotations
            .entry(*a.slice() as usize)
            .or_insert(HashMap::<String, Annotation>::new())
            .insert(a.label().to_string(), a);
    });
    let max_gap = 100;
    let layers = construct_layers(
        grouped_annotations, x, z, max_gap, &layer_order, 0
    );
/*     let layers: HashMap<String, Array2<f64>> = grouped_annotations.into_par_iter()
        .map(|(k, anns)| (k, construct_layer(anns, x, z, max_gap)))
        .collect();
    let pylayers: HashMap<String, &PyArray2<f64>> = layers.into_iter()
        .map(|(k, layer)| (k, layer.into_pyarray(py)))
        .collect(); */
    let out: HashMap<String, &PyArray2<f64>> = layer_thicknesses(layers, &layer_order).into_iter()
        .map(|(k, layer)| (k, layer.into_pyarray(py)))
        .collect();
    //Ok(layers.into_pyarray(py))
    Ok(out)
}

#[pyfunction]
fn process_volumes(py: Python<'_>, annotations: String, x: usize, y: usize, z: usize) -> PyResult<&PyArray3<i16>> {
    let mut s = String::new();
    File::open(annotations).unwrap().read_to_string(&mut s).unwrap();
    let annotations: Vec<Annotation> = serde_json::from_str(&s).unwrap();
    let mut grouped_annotations = HashMap::<usize, Vec<Annotation>>::new();
    annotations.into_iter().for_each(|a| {
        let vec = grouped_annotations
            .entry(*a.slice() as usize)
            .or_insert(Vec::<Annotation>::new());
        vec.push(a);
    });

    let mut array = Array::<i16, _>::zeros((x, y, z));
    array.axis_iter_mut(Axis(2))
        //.into_par_iter()
        .enumerate()
        .for_each(|(idx, mut slice)|{
            let slice_annotations = grouped_annotations.get(&idx);
            // No annotations at this layer
            if slice_annotations.is_none() { return }
            let mut r = BinaryBuilder::new()
                .height(slice.shape()[0])
                .width(slice.shape()[1])
                .build().unwrap();
            for a in slice_annotations.unwrap() {
                let points = a.data.handles.points().unwrap();
                let ls: LineString<i16> = LineString::from(points.iter()
                    //Hmm not sure why x/y switched is necessary here but it is
                    .map(|p| (p.y.floor() as i16, p.x.floor() as i16))
                    .collect::<Vec<(i16, i16)>>()    
                );
                let poly = Polygon::new(ls, vec![]);
                r.rasterize(&poly).unwrap();
            }
            let buffer = r.finish().map(|v| *v as i16);
            Zip::from(&mut slice)
                .and(&buffer)
                .for_each(|res, buf|{
                    if *buf != 0 {
                        *res = *buf
                    }
                });
        });
    Ok(array.into_pyarray(py))

}

/// A Python module implemented in Rust.
#[pymodule]
fn annotation_handler(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(sum_as_string, m)?)?;
    m.add_function(wrap_pyfunction!(process_volumes, m)?)?;
    m.add_function(wrap_pyfunction!(process_layers, m)?)?;
    Ok(())
}
