use ndarray::{Array2, Array, Axis, s};
use serde::{Deserialize};
use ndarray_stats::QuantileExt;

#[derive(Debug, Deserialize)]
pub struct Annotation {
    _id: String,
    pub data: Data,
}

impl Annotation {
    pub fn label(&self) -> &String {
        &self.data.location
    }

    pub fn slice(&self) -> &i32 {
        &self.data.sliceNumber
    }
}

#[derive(Debug, Deserialize)]
pub struct Data {
    toolType: String,
    frameIndex: i32,
    lesionNamingNumber: i32,
    location: String,
    measurementNumber: i32,
    pub handles: AnnotationHandles,
    sliceNumber: i32,
}


#[derive(Debug, Deserialize, Clone)]
pub struct Point {
    pub x: f64,
    pub y: f64,
}

#[derive(Debug, Deserialize)]
#[serde(untagged)]
pub enum AnnotationHandles {
    FreehandHandles {points: Vec<Point>},
    OpenFreehandHandles {points: Vec<Vec<Point>>},
    //CircleHandles{start: Point, end: Point, initialRotation: usize}
}

impl AnnotationHandles {
    pub fn points(&self) -> Option<&Vec<Point>> {
        match self {
            Self::FreehandHandles{ points} => Some(points),
            Self::OpenFreehandHandles{..} => None
        }

    }

    pub fn lines(&self) -> Option<Vec<((Point, Point), Array2<f64>)>> {
        match self {
            Self::FreehandHandles{ .. } => None,
            Self::OpenFreehandHandles{points} => {
                let mut lines = Vec::<((Point, Point), Array2<f64>)>::new();
                for line in points.into_iter() {
                    let mut arr = Array2::<f64>::zeros((line.len(), 2));
                    for (i , mut row) in arr.axis_iter_mut(Axis(0)).enumerate(){
                        let point = &line[i];
                        row.assign(&Array::from(vec![point.x, point.y]));
                    }
                    let min_loc: usize = arr.slice(s![..,0]).argmin().unwrap();
                    let max_loc: usize = arr.slice(s![..,0]).argmax().unwrap();
                    let min = arr.slice(s![min_loc,..]);
                    let max = arr.slice(s![max_loc,..]);
                    lines.push(((
                        Point{x: min[0], y: min[1]},
                        Point{x: max[0], y: max[1]},
                    ), arr));
                }
                Some(lines)
            }
        }
    }
}
