use std::collections::HashMap;

use ndarray::{prelude::s, Zip, Array3, Axis, ArrayView, Ix1, Ix2, ArrayViewMut};
use interp1d::Interp1d;
use ndarray::parallel::prelude::*;

use crate::annotations::Annotation;
//use rayon::prelude::*;
use ndarray::{Array, Array2, Array1};
use itertools::Itertools;

struct InterpolationInfo {
    max_gap: usize,
    fill_value: usize
}

// Note: max_gap must be larger than 2 pixels.
pub fn construct_layer(
    annotations: Vec<Annotation>, x: usize, z: usize,  max_gap: usize
) -> Array2<f64> {
    let mut layer = Array2::<f64>::from_elem((x, z), -1.);
    for (o_idx, a) in annotations.iter().enumerate() {
        let mut slice = layer.slice_mut(s![..,*a.slice() - 1]);
        let lines = a.data.handles.lines().unwrap();
        for (idx, ((min, max),segment)) in lines.iter().enumerate() {
            // Get last point of current segment and first point of next segment
            let x_min = min.x.floor() as usize;
            let x_max = max.x.floor() as usize;
            let mut domain  = slice.slice_mut(
                s![x_min..x_max]
            );

            let f = Interp1d::new_unsorted(
                segment.slice(s![.., 0]).to_vec(),
                segment.slice(s![.., 1]).to_vec(),
            ).unwrap();
            
            let y: Array1<f64> = Zip::from(&Array::range(x_min as f64, x_max as f64,1.0))
                .map_collect(|v| f.interpolate_checked(*v).unwrap_or(-1.0));
            domain.assign(&y);
        }
        let gaps: Vec<usize> = slice.indexed_iter()
            .filter_map(|(idx, x)| if *x == -1.0 {Some(idx)} else {None})
            .collect();
        let mut start_ptr: Option<usize> = None;
        for (i, gap ) in gaps.iter().enumerate() {
            if start_ptr.is_none() {
                start_ptr = Some(i);
                continue
            }
            let start = start_ptr.unwrap();
            // Not at end and current gap is monotonically increasing.
            if i < gaps.len() - 1 && gaps[i + 1] - gap == 1 { continue }
            // Do nothing for too big of a gap
            if i - start > max_gap { continue }

            // Close the gap
            let start_idx = if gaps[start] == 0 { 0 } else {gaps[start] - 1};
            let end_idx = *gap + 1;
            if start_idx < 1 || end_idx > slice.shape()[0] - 1 { continue }
            let start_y = slice[start_idx];
            let end_y = slice[end_idx + 1];
            let f = Interp1d::new_sorted(
                vec![start_idx as f64, end_idx as f64],
                vec![start_y, end_y]
            ).unwrap();
            let mut domain  = slice.slice_mut(
                s![start_idx..end_idx]
            );
            let y: Array1<f64> = Zip::from(&Array::range(
                start_idx as f64, end_idx as f64,1.0
            ))
                .map_collect(|v| f.interpolate(*v));
            domain.assign(&y);

        }
    }
    layer
}

pub fn construct_layers(
    full_annotations: HashMap::<usize, HashMap<String, Annotation>>,
    x: usize, z: usize, max_gap: usize,
    layer_order: &Vec<String>, fill_value: usize
) -> Array3<f64> {
    let mut layers = Array3::<f64>::from_elem((x, z, layer_order.len()), -1.);

    layers.axis_iter_mut(Axis(1))
        //.into_par_iter()
        .enumerate()
        .for_each(|(idx, mut b_slice)|{
            let annotations = full_annotations.get(&idx);
            // No annotations at this layer
            if annotations.is_none() { return }

            let annotations = annotations.unwrap();
            let mut layer_interp_fns = Vec::<Interp1d<f64, f64>>::new();
            for (layer_num, layer) in layer_order.iter().enumerate() {
                // Assume only one annotation per slice per layer, validation elsewhere
                let layer_annotation = annotations.get(layer).unwrap();
                let line_data = layer_annotation.data.handles.lines().unwrap();

                // For each segment, interpolate points to each pixel (only in x)
                for line in line_data.iter() {
                    let ((min, max), segment) = line;
                    let min_x = min.x.floor() as usize;
                    let max_x = max.x.floor() as usize;
                    let f = Interp1d::new_unsorted(
                        segment.slice(s![.., 0]).to_vec(),
                        segment.slice(s![.., 1]).to_vec()
                    ).unwrap();
                    let mut domain  = b_slice.slice_mut(
                        s![min_x..max_x, layer_num]
                    );
                    
                    let y: Array1<f64> = Zip::from(&Array::range(min_x as f64, max_x as f64,1.0))
                        .map_collect(|v| f.interpolate_checked(*v).unwrap_or(-1.0));
                    domain.assign(&y);
                }
                let gap_idxs = find_gaps(&b_slice.slice(s![.., layer_num]));
                let mut start: Option<usize> = None;
                for (gap_num, gap_idx ) in gap_idxs.iter().enumerate() {
                    // Keep track of where current gap started
                    if start.is_none() {
                        start = Some(gap_num);
                    }
                    let start_idx = start.unwrap();

                    // Not at end and current gap is monotonically increasing.
                    let not_at_end = gap_num < (gap_idxs.len() - 1);
                    if not_at_end && (gap_idxs[gap_num + 1] - gap_idx) == 1{continue}

                    // Get x indices of the gap start and the gap end
                    let gap_start = if gap_idxs[start_idx] == 0 { 0 } else {
                        gap_idxs[start_idx] - 1
                    };
                    let gap_end = *gap_idx + 1;
                    // Don't handle because the gap is at the start or the end
                    if gap_start < 1 || gap_end > b_slice.shape()[0] - 1 { 
                        start = None;
                        continue 
                    }
                    let interp_inf = InterpolationInfo{
                        max_gap,
                        fill_value
                    };
                    close_gap(
                        &mut b_slice, &layer_interp_fns,
                        gap_start, gap_end, layer_num,
                        interp_inf,
                    );
                    start = None;
                }
                // Gaps have been closed, now we can build the whole layer interpolation function
                let f = Interp1d::new_unsorted(
                    Array::range(0., b_slice.shape()[0] as f64, 1.0).to_vec(),
                    b_slice.slice(s![.., layer_num]).to_vec()
                ).unwrap();
                layer_interp_fns.push(f);
            }
        });
    layers
}

fn close_gap(
    slice: &mut ArrayViewMut<f64, Ix2>,
    layer_interp_fns: &Vec<Interp1d<f64, f64>>,
    gap_start: usize,
    gap_end: usize,
    layer_num: usize, 
    info: InterpolationInfo
) {
    let start_y = slice[[gap_start, layer_num]];
    let end_y = slice[[gap_end, layer_num]];
    let mut gap_domain = slice.slice_mut(
        s![gap_start..gap_end, layer_num]
    );
    // Interpolate from previous layer function
    if gap_end - gap_start > info.max_gap {
        // Anatomically bottom layer, don't interpolate from previous
        // Instead fill with fill_value
        if layer_num == 0 {
            gap_domain.fill(info.fill_value as f64);
        } else {
            let f = &layer_interp_fns[layer_num - 1];
            let y = Zip::from(&Array::range(
                    gap_start as f64,
                    gap_end as f64, 1.0
                ))
                .map_collect(|v| f.interpolate(*v));
            gap_domain.assign(&y);
        }
        return
    }
    // Close the small gap
    let f = Interp1d::new_sorted(
        vec![gap_start as f64, gap_end as f64],
        vec![start_y, end_y]
    ).unwrap();
    let y: Array1<f64> = Zip::from(&Array::range(
        gap_start as f64, gap_end as f64,1.0
    ))
        .map_collect(|v| f.interpolate(*v));
    gap_domain.assign(&y);
}


fn find_gaps(slice: &ArrayView<f64, Ix1>) -> Vec<usize>{
    // After adding each segment, check for gaps
    slice.indexed_iter()
        .filter_map(|(idx, x)| if *x == -1.0 {Some(idx)} else {None})
        .collect()
}

pub fn layer_thicknesses(layers: Array3<f64>, layer_order: &Vec<String>) -> HashMap<String, Array2<f64>> {
    let mut combos = HashMap::<String, Array2<f64>>::new();
    for idxs in (0..layers.shape()[2]).combinations(2) {
        let sh = layers.shape();
        let mut res = Array2::zeros((sh[0], sh[1]));

        Zip::from(&mut res)
            .and(&layers.slice(s![.., .., idxs[0]]))
            .and(&layers.slice(s![.., .., idxs[1]]))
            .par_for_each(|res, el1, el2|{
                if *el1 == -1.0 || *el2 == -1.0 {
                    *res = f64::NAN;
                } else {
                    *res = *el2 - *el1;
                }
            });
        let key = format!("{}_{}", layer_order[idxs[0]], layer_order[idxs[1]]);
        combos.insert(key, res);
    }
    combos
}