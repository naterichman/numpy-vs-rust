import json
from annotation_handler import process_layers
from scipy.interpolate import interp1d
import time
import numpy as np
from matplotlib import pyplot as plt
import itertools

layer_order = ["BM","IB-RPE","EZ","ELM","OPL-HFL","ILM"]

def construct_layers_v1(filename, x, _y,z):
    with open(filename, 'r') as fp:
        annotations = json.load(fp)
    max_gap = 100
    grouped_annotations = dict()
    for annotation in annotations:
        location = annotation['data']['location']
        a_list = grouped_annotations.setdefault(location, [])
        a_list.append(annotation)
    layers = dict()
    for name, anns in grouped_annotations.items():
        layer = np.full((x,z), -1.0)
        for i, annotation in enumerate(anns):
            data = annotation["data"]
            loc = annotation['data']['sliceNumber'] - 1
            for segment in data['handles']['points']:
                points = np.array(
                    [[p["x"], p["y"]] for p in segment]
                )
                min_ = np.argmin(points[:, 0])
                max_ = np.argmax(points[:, 0])
                x_min = int(np.floor(points[min_, 0]))
                x_max = int(np.floor(points[max_, 0]))
                f = interp1d(
                    points[:, 0], points[:, 1], bounds_error=False, fill_value=-1
                )
                layer[x_min:x_max,loc] = f(
                    np.arange(x_min, x_max)
                )
            gaps = np.where(layer[:,loc] == -1)[0]
            if gaps.shape[0] > 0:
                start = None
                for i, gap_idx in enumerate(gaps):
                    if start is None:
                        start = i
                        continue
                    if i < len(gaps) - 1 and gaps[i + 1] - gap_idx == 1:
                        continue
                    if i - start > max_gap:
                        continue

                    start_idx = gaps[start] - 1
                    end_idx = gap_idx + 1
                    if start_idx < 1 or end_idx > layer.shape[0] - 1:
                        continue
                    start_y = layer[start_idx, loc]
                    end_y = layer[end_idx, loc]
                    f = interp1d(
                            [start_idx, end_idx],
                            [start_y, end_y]
                    )
                    layer[start_idx:end_idx, loc] = f(
                            np.arange(start_idx, end_idx)
                    )
        layers[name] = layer

    return layers

def construct_layers_v2(filename, x, _y, z):
    with open(filename, 'r') as fp:
        annotations = json.load(fp)
    max_gap = 100
    fill_value = 0
    grouped_annotations = dict()
    for annotation in annotations:
        location = annotation['data']['location']
        slice_ = annotation['data']['sliceNumber']
        a = grouped_annotations.setdefault(slice_, {})
        a[location] = annotation

    layers = np.full((x, z, len(layer_order)), -1.0)
    for idx in range(layers.shape[1]):
        b_slice = layers[:,idx,:]
        layer_interp_fns = []
        annotations = grouped_annotations.get(idx)
        if not annotations:
            continue
        for layer_num, layer in enumerate(layer_order):
            layer_annotation = annotations.get(layer)
            data = layer_annotation["data"]
            #if idx == 15 and layer_num == 2:
            #    breakpoint()
            for segment in data['handles']['points']:
                points = np.array(
                    [[p["x"], p["y"]] for p in segment]
                )
                x_min = np.floor(np.min(points[:, 0]))
                x_max = np.floor(np.max(points[:, 0]))
                f = interp1d(
                    points[:, 0], points[:, 1], bounds_error=False, fill_value=-1
                )
                b_slice[int(x_min):int(x_max),layer_num] = f(
                    np.arange(x_min, x_max)
                )
            gap_idxs = np.where(b_slice[:,layer_num] == -1)[0]
            if gap_idxs.shape[0] > 0:
                start_idx = None
                for gap_num, gap_idx in enumerate(gap_idxs):
                    if start_idx is None:
                        start_idx = gap_num
                    if gap_num < (len(gap_idxs) - 1) and (gap_idxs[gap_num + 1] - gap_idx) == 1:
                        continue
                    gap_start = gap_idxs[start_idx] - 1
                    gap_end = gap_idx + 1
                    if gap_start < 1 or gap_end > b_slice.shape[0] - 1:
                        start_idx = None
                        continue
                    start_y = b_slice[gap_start, layer_num]
                    end_y = b_slice[gap_end, layer_num]
                    if gap_end - gap_start > max_gap:
                        if layer_num == 0:
                            b_slice[gap_start:gap_end, layer_num] = fill_value
                        else:
                            f = layer_interp_fns[layer_num - 1]
                            b_slice[gap_start:gap_end, layer_num] = f(np.arange(gap_start, gap_end))
                    else:
                        f = interp1d(
                            [gap_start, gap_end],
                            [start_y, end_y]
                        )
                        b_slice[gap_start:gap_end, layer_num] = f(np.arange(gap_start, gap_end))
                    start_idx = None
            # Gaps have been filled,
            f = interp1d(
                np.arange(0, b_slice.shape[0]),
                b_slice[:, layer_num]
            )
            layer_interp_fns.append(f)
    return layers

def perform_layer_calculations(layers):
    combos = {}
    for (ix1, ix2) in itertools.combinations(range(layers.shape[-1]), 2):
        layer1 = layers[:,:,ix1]
        layer2 = layers[:,:,ix2]
        undef1 = np.where(layer1 == -1)
        undef2 = np.where(layer2 == -1)

        res = layer2 - layer1
        res[undef1] = np.nan
        res[undef2] = np.nan
        key = f"{layer_order[ix1]}_{layer_order[ix2]}"
        combos[key] = res

    return combos


if __name__ == '__main__':
    fname = './layers.json'
    with open(fname, 'r') as fp:
        annotations = json.load(fp)

# We are going to increase this as the code gets faster and faster.
    NUM_ITER = 100
    #fig, ax = plt.subplots(4, 4)
    #ax = ax.ravel()

    t0 = time.perf_counter()
    for _ in range(NUM_ITER):
        out = process_layers(fname, 1024, 496, 97)
        #print("\n".join([f"{k}: {v.shape}" for k,v in out.items()]))
    t1 = time.perf_counter()
    #for i, (k, v) in enumerate(out.items()):
    #    ax[i].set_title(f"{k}-rust")
    #    ax[i].imshow(v, aspect='auto', interpolation='none')

    took = (t1 - t0) / NUM_ITER
    print(f"Rust: took and avg of {took * 1000:.2f}ms per iteration ({NUM_ITER} iterations)")
    fig, ax = plt.subplots(4, 4)
    ax = ax.ravel()

    t0 = time.perf_counter()
    for _ in range(NUM_ITER):
        out = construct_layers_v2(fname, 1024, 496, 97)
        res = perform_layer_calculations(out)
        #print("\n".join([f"{k}: {v.shape}" for k,v in res.items()]))
    #for i, (k, v) in enumerate(res.items()):
    #    ax[i].set_title(f"{k}-python")
    #    ax[i].imshow(v, aspect='auto', interpolation='none')
    #ax[2].imshow(out['BM'], aspect='auto', vmin=100, vmax=300, interpolation='none')
    #ax[2].set_title('python-BM')
    #ax[3].imshow(out['EZ'], aspect='auto', vmin=100, vmax=300, interpolation='none')
    #ax[3].set_title('python-EZ')
    t1 = time.perf_counter()
    took = (t1 - t0) / NUM_ITER
    print(f"Python: took and avg of {took * 1000:.2f}ms per iteration ({NUM_ITER} iterations)")
    #plt.show()
